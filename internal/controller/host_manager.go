package controller

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"math/big"
	"os"
	"regexp"
	"strings"
	"sync"

	"github.com/go-logr/logr"
	bmov1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	"github.com/pkg/errors"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/cache"
	"k8s.io/utils/pointer"
	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/cluster-api/util/conditions"
	"sigs.k8s.io/cluster-api/util/patch"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type HostManagerInterface interface {
	SetFinalizer()
	UnsetFinalizer()
	IsProvisioned() bool
	Associate(context.Context) error
	Delete(context.Context) error
	Update(context.Context) error
	SetError(string, hostv1alpha1.HostStatusError)
	SetPauseAnnotation(context.Context) error
	RemovePauseAnnotation(context.Context) error
	GetBaremetalHostID(context.Context) (*string, error)
	HasAnnotation() bool
	SetConditionHostToFalse(clusterv1.ConditionType, string, clusterv1.ConditionSeverity, string, ...interface{})
	SetConditionHostToTrue(clusterv1.ConditionType)
}

type HostManager struct {
	client client.Client
	Host   *hostv1alpha1.Host
	Log    logr.Logger
}

const (
	// PausedAnnotationKey is an annotation to be used for pausing a BMH.
	PausedAnnotationKey = "kanod.io/hostmgr"
	// HostAnnotation is the key for an annotation that should go on a Host to
	// reference what BareMetalHost it corresponds to.
	HostAnnotation = "metal3.io/BareMetalHost"
	// UnhealthyAnnotation is the annotation used by the Metal3Health
	// that sets unhealthy status of BMH.
	UnhealthyAnnotation = "capi.metal3.io/unhealthy"
)

var (
	// Capm3FastTrack is the variable fetched from the CAPM3_FAST_TRACK environment variable.
	Capm3FastTrack = os.Getenv("CAPM3_FAST_TRACK")
	// a mutex to perform one association at a time
	associateBMHMutex sync.Mutex
	// a regexp to skip/select labels that are handled by the host operator.
	matchHostDomainRe = regexp.MustCompile(`^host.kanod.io/`)
)

func NewHostManager(client client.Client, log logr.Logger, host *hostv1alpha1.Host) (*HostManager, error) {
	return &HostManager{
		client: client,
		Host:   host,
		Log:    log,
	}, nil
}

// SetError sets the ErrorMessage and ErrorReason fields on the machine and logs
// the message. It assumes the reason is invalid configuration, since that is
// currently the only relevant MachineStatusError choice.
func (m *HostManager) SetError(message string, reason hostv1alpha1.HostStatusError) {
	m.Host.Status.FailureMessage = &message
	m.Host.Status.FailureReason = &reason
}

// clearError removes the ErrorMessage from the machine's Status if set. Returns
// nil if ErrorMessage was already nil. Returns a RequeueAfterError if the
// machine was updated.
func (m *HostManager) clearError() {
	if m.Host.Status.FailureMessage != nil || m.Host.Status.FailureReason != nil {
		m.Host.Status.FailureMessage = nil
		m.Host.Status.FailureReason = nil
	}
}

func (m *HostManager) SetPauseAnnotation(ctx context.Context) error {
	// look for associated BMH
	host, helper, err := m.getBmh(ctx)
	if err != nil {
		m.SetError("Failed to get a BaremetalHost for the Host",
			hostv1alpha1.UpdateHostError,
		)
		return err
	}
	if host == nil {
		return nil
	}

	annotations := host.GetAnnotations()

	if annotations != nil {
		if _, ok := annotations[bmov1alpha1.PausedAnnotation]; ok {
			m.Log.Info("BaremetalHost is already paused")
			return nil
		}
	} else {
		host.Annotations = make(map[string]string)
	}
	m.Log.Info("Adding PausedAnnotation in BareMetalHost")
	host.Annotations[bmov1alpha1.PausedAnnotation] = PausedAnnotationKey

	// Setting annotation with BMH status
	newAnnotation, err := json.Marshal(&host.Status)
	if err != nil {
		m.SetError("Failed to marshal the BareMetalHost status",
			hostv1alpha1.UpdateHostError,
		)
		return errors.Wrap(err, "failed to marshall status annotation")
	}
	obj := map[string]interface{}{}
	if err := json.Unmarshal(newAnnotation, &obj); err != nil {
		return errors.Wrap(err, "failed to unmarshall status annotation")
	}
	delete(obj, "hardware")
	newAnnotation, _ = json.Marshal(obj)
	host.Annotations[bmov1alpha1.StatusAnnotation] = string(newAnnotation)
	return helper.Patch(ctx, host)
}

func (m *HostManager) RemovePauseAnnotation(ctx context.Context) error {
	// look for associated BMH
	bmh, helper, err := m.getBmh(ctx)
	if err != nil {
		m.SetError("Failed to get a BaremetalHost for the Host",
			hostv1alpha1.CreateHostError,
		)
		return err
	}

	if bmh == nil {
		return nil
	}

	annotations := bmh.GetAnnotations()

	if annotations != nil {
		if _, ok := annotations[bmov1alpha1.PausedAnnotation]; ok {
			if annotations[bmov1alpha1.PausedAnnotation] == PausedAnnotationKey {
				// Removing BMH Paused Annotation Since Owner Cluster is not paused.
				delete(bmh.Annotations, bmov1alpha1.PausedAnnotation)
			} else {
				m.Log.Info("BMH is paused by user. Not removing Pause Annotation")
				return nil
			}
		}
	}
	return helper.Patch(ctx, bmh)
}

// getBmh gets the associated BareMetalHost by looking for an annotation on the machine
// that contains a reference to the host. Returns nil if not found. Assumes the
// host is in the same namespace as the machine.
func (m *HostManager) getBmh(ctx context.Context) (*bmov1alpha1.BareMetalHost, *patch.Helper, error) {
	host, err := getBmh(ctx, m.Host, m.client, m.Log)
	if err != nil || host == nil {
		return host, nil, err
	}
	helper, err := patch.NewHelper(host, m.client)
	return host, helper, err
}

func getBmh(ctx context.Context, m3Machine *hostv1alpha1.Host, cl client.Client,
	mLog logr.Logger,
) (*bmov1alpha1.BareMetalHost, error) {
	annotations := m3Machine.ObjectMeta.GetAnnotations()
	if annotations == nil {
		return nil, nil
	}
	hostKey, ok := annotations[BmhAnnotation]
	if !ok {
		return nil, nil
	}
	hostNamespace, hostName, err := cache.SplitMetaNamespaceKey(hostKey)
	if err != nil {
		mLog.Error(err, "Error parsing annotation value", "annotation key", hostKey)
		return nil, err
	}

	host := bmov1alpha1.BareMetalHost{}
	key := client.ObjectKey{
		Name:      hostName,
		Namespace: hostNamespace,
	}
	err = cl.Get(ctx, key, &host)
	if apierrors.IsNotFound(err) {
		mLog.Info("Annotated host not found", "host", hostKey)
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return &host, nil
}

// SetFinalizer sets finalizer on the host
func (m *HostManager) SetFinalizer() {
	// If the Host doesn't have finalizer, add it.
	if !Contains(m.Host.Finalizers, hostv1alpha1.HostFinalizer) {
		m.Host.Finalizers = append(m.Host.Finalizers,
			hostv1alpha1.HostFinalizer,
		)
	}

}

// UnsetFinalizer unsets finalizer on the host
func (m *HostManager) UnsetFinalizer() {
	// Cluster is deleted so remove the finalizer.
	m.Host.Finalizers = Filter(m.Host.Finalizers,
		hostv1alpha1.HostFinalizer,
	)

}

func (m *HostManager) IsProvisioned() bool {
	return m.Host.Status.Ready
}

func (m *HostManager) Associate(ctx context.Context) error {
	// Parallel attempts to associate is problematic since the same BMH
	// could be selected for multiple Hosts. Therefore we use a mutex lock here.
	associateBMHMutex.Lock()
	defer associateBMHMutex.Unlock()
	m.Log.Info("Associating host", "host", m.Host.Name)

	// load and validate the config
	if m.Host == nil {
		// Should have been picked earlier. Do not requeue
		return nil
	}

	// clear an error if one was previously set
	m.clearError()

	// look for associated BMH
	bmh, helper, err := m.getBmh(ctx)
	if err != nil {
		m.SetError("Failed to get the BaremetalHost for the Host",
			hostv1alpha1.CreateHostError,
		)
		return err
	}

	// no BMH found, trying to choose from available ones
	if bmh == nil {
		bmh, helper, err = m.chooseBMH(ctx)
		if err != nil {
			if ok := errors.As(err, &hasRequeueAfterError); !ok {
				m.SetError("Failed to pick a BaremetalHost for the Host",
					hostv1alpha1.CreateHostError,
				)
			}
			return err
		}
		if bmh == nil {
			m.Log.Info("No available host found. Requeuing.")
			return &RequeueAfterError{RequeueAfter: requeueAfter}
		}
		m.Log.Info("Associating machine with host", "host", bmh.Name)
	} else {
		m.Log.Info("Machine already associated with host", "host", bmh.Name)
	}

	/*
		err = m.setHostLabel(ctx, bmh)
		if err != nil {
			if ok := errors.As(err, &hasRequeueAfterError); !ok {
				m.SetError("Failed to set the Cluster label in the BareMetalHost",
					hostv1alpha1.CreateHostError,
				)
			}
			return err
		}
	*/

	err = m.setHostConsumerRef(bmh)
	if err != nil {
		if ok := errors.As(err, &hasRequeueAfterError); !ok {
			m.SetError("Failed to associate the BaremetalHost to the Host",
				hostv1alpha1.CreateHostError,
			)
		}
		return err
	}

	if err = m.setBmhSpec(bmh); err != nil {
		if ok := errors.As(err, &hasRequeueAfterError); !ok {
			m.SetError("Failed to associate the BaremetalHost to the Host",
				hostv1alpha1.CreateHostError,
			)
		}
		return err
	}

	m.syncInfo(bmh)

	err = helper.Patch(ctx, bmh)
	if err != nil {
		var aggr kerrors.Aggregate
		if ok := errors.As(err, &aggr); ok {
			for _, kerr := range aggr.Errors() {
				if apierrors.IsConflict(kerr) {
					return &RequeueAfterError{}
				}
			}
		}
		return err
	}

	err = m.ensureAnnotation(bmh)
	if err != nil {
		if ok := errors.As(err, &hasRequeueAfterError); !ok {
			m.SetError("Failed to annotate the Host",
				hostv1alpha1.CreateHostError,
			)
		}
		return err
	}

	m.Log.Info("Finished associating machine")
	return nil
}

// Update updates a machine and is invoked by the Machine Controller.
func (m *HostManager) Update(ctx context.Context) error {
	m.Log.Info("Updating machine")

	// clear any error message that was previously set. This method doesn't set
	// error messages yet, so we know that it's incorrect to have one here.
	m.clearError()

	bmh, helper, err := m.getBmh(ctx)
	if err != nil {
		return err
	}
	if bmh == nil {
		return errors.Errorf("bmh not found for host %s", m.Host.Name)
	}

	// ensure that the BMH specs are correctly set.
	err = m.setBmhConsumerRef(bmh)
	if err != nil {
		if ok := errors.As(err, &hasRequeueAfterError); !ok {
			m.SetError("Failed to associate the BaremetalHost to the Host",
				hostv1alpha1.CreateHostError,
			)
		}
		return err
	}

	// ensure that the BMH specs are correctly set.
	err = m.setBmhSpec(bmh)
	if err != nil {
		if ok := errors.As(err, &hasRequeueAfterError); !ok {
			m.SetError("Failed to associate the BaremetalHost to the Host",
				hostv1alpha1.CreateHostError,
			)
		}
		return err
	}

	m.syncInfo(bmh)

	err = helper.Patch(ctx, bmh)
	if err != nil {
		return err
	}

	err = m.ensureAnnotation(bmh)
	if err != nil {
		return err
	}

	if err := m.updateHostStatus(bmh); err != nil {
		return err
	}
	m.Host.Status.Synced = true

	m.Log.Info("Finished updating machine")
	return nil
}

func (m *HostManager) setBmhConsumerRef(bmh *bmov1alpha1.BareMetalHost) error {
	bmh.Spec.ConsumerRef = &corev1.ObjectReference{
		Kind:       "Host",
		Name:       m.Host.Name,
		Namespace:  m.Host.Namespace,
		APIVersion: m.Host.APIVersion,
	}

	/* TODO NodeReuse
	// Delete nodeReuseLabelName from host.
	m.Log.Info("Deleting nodeReuseLabelName from host, if any")

	labels := bmh.GetLabels()
	if labels != nil {
		if _, ok := labels[nodeReuseLabelName]; ok {
			delete(bmh.Labels, nodeReuseLabelName)
			m.Log.Info("Finished deleting nodeReuseLabelName")
		}
	}
	*/

	return nil
}

// SetConditionHostToFalse sets Host condition status to False.
func (m *HostManager) SetConditionHostToFalse(t clusterv1.ConditionType, reason string, severity clusterv1.ConditionSeverity, messageFormat string, messageArgs ...interface{}) {
	conditions.MarkFalse(m.Host, t, reason, severity, messageFormat, messageArgs...)
}

// SetConditionHostToTrue sets Host condition status to True.
func (m *HostManager) SetConditionHostToTrue(t clusterv1.ConditionType) {
	conditions.MarkTrue(m.Host, t)
}

// ensureAnnotation makes sure the machine has an annotation that references the
// host and uses the API to update the machine if necessary.
func (m *HostManager) ensureAnnotation(host *bmov1alpha1.BareMetalHost) error {
	annotations := m.Host.ObjectMeta.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}
	hostKey, err := cache.MetaNamespaceKeyFunc(host)
	if err != nil {
		m.Log.Error(err, "Error parsing annotation value", "annotation key", hostKey)
		return err
	}
	existing, ok := annotations[BmhAnnotation]
	if ok {
		if existing == hostKey {
			return nil
		}
		m.Log.Info("Warning: found stray annotation for host on machine. Overwriting.", "host", existing)
	}
	annotations[BmhAnnotation] = hostKey
	m.Host.ObjectMeta.SetAnnotations(annotations)

	return nil
}

// setBmhSpec will ensure the host's Spec is set according to the machine's
// details. It will then update the host via the kube API. If UserData does not
// include a Namespace, it will default to the Host's namespace.
func (m *HostManager) setBmhSpec(bmh *bmov1alpha1.BareMetalHost) error {
	// We only want to update the image setting if the host does not
	// already have an image.
	//
	// A host with an existing image is already provisioned and
	// upgrades are not supported at this time. To re-provision a
	// host, we must fully deprovision it and then provision it again.
	// Not provisioning while we do not have the UserData.
	if bmh.Spec.Image == nil && m.Host.Spec.UserData != nil {
		checksumType := ""
		if m.Host.Spec.Image.ChecksumType != nil {
			checksumType = *m.Host.Spec.Image.ChecksumType
		}
		bmh.Spec.Image = &bmov1alpha1.Image{
			URL:          m.Host.Spec.Image.URL,
			Checksum:     m.Host.Spec.Image.Checksum,
			ChecksumType: bmov1alpha1.ChecksumType(checksumType),
			DiskFormat:   m.Host.Spec.Image.DiskFormat,
		}

		// TODO: We may want to COPY secret in the BMH space.
		// This would require some support to automate on the three fields.

		if m.Host.Spec.UserData != nil {
			bmh.Spec.UserData = m.Host.Spec.UserData.DeepCopy()
			if bmh.Spec.UserData.Namespace == "" {
				bmh.Spec.UserData.Namespace = m.Host.Namespace
			}
		} else {
			bmh.Spec.UserData = nil
		}

		// Set metadata from gathering from Spec.metadata and from the template.
		if m.Host.Spec.MetaData != nil {
			bmh.Spec.MetaData = m.Host.Spec.MetaData.DeepCopy()
			if bmh.Spec.MetaData.Namespace == "" {
				bmh.Spec.MetaData.Namespace = m.Host.Namespace
			}
		} else {
			bmh.Spec.MetaData = nil
		}
		if m.Host.Spec.NetworkData != nil {
			bmh.Spec.NetworkData = m.Host.Spec.NetworkData
			if bmh.Spec.NetworkData.Namespace == "" {
				bmh.Spec.NetworkData.Namespace = m.Host.Namespace
			}
		} else {
			bmh.Spec.NetworkData = nil
		}
	}
	/* TODO: It is not clear this should be offered. We may reconsider it.

	// Set automatedCleaningMode from host.spec.automatedCleaningMode.
	if m.Host.Spec.AutomatedCleaningMode != nil {
		if bmh.Spec.AutomatedCleaningMode != bmov1alpha1.AutomatedCleaningMode(*m.Host.Spec.AutomatedCleaningMode) {
			bmh.Spec.AutomatedCleaningMode = bmov1alpha1.AutomatedCleaningMode(*m.Host.Spec.AutomatedCleaningMode)
		}
	}
	*/

	bmh.Spec.Online = m.Host.Spec.Online
	return nil
}

// updateHostStatus updates a Host object's status.
func (m *HostManager) updateHostStatus(bmh *bmov1alpha1.BareMetalHost) error {
	addrs := m.nodeAddresses(bmh)

	hostOld := m.Host.DeepCopy()

	m.Host.Status.Addresses = addrs
	conditions.MarkTrue(m.Host, hostv1alpha1.AssociateBMHCondition)

	if equality.Semantic.DeepEqual(m.Host.Status, hostOld.Status) {
		// Status did not change
		return nil
	}

	now := metav1.Now()
	m.Host.Status.LastUpdated = &now
	return nil
}

func (m *HostManager) nodeAddresses(bmh *bmov1alpha1.BareMetalHost) []hostv1alpha1.HostAddress {
	addrs := []hostv1alpha1.HostAddress{}

	// If the host is nil or we have no hw details, return an empty address array.
	if bmh == nil || bmh.Status.HardwareDetails == nil {
		return addrs
	}

	for _, nic := range bmh.Status.HardwareDetails.NIC {
		address := hostv1alpha1.HostAddress{
			Type:    hostv1alpha1.HostInternalIP,
			Address: nic.IP,
		}
		addrs = append(addrs, address)
	}

	if bmh.Status.HardwareDetails.Hostname != "" {
		addrs = append(addrs, hostv1alpha1.HostAddress{
			Type:    hostv1alpha1.HostHostName,
			Address: bmh.Status.HardwareDetails.Hostname,
		})
		addrs = append(addrs, hostv1alpha1.HostAddress{
			Type:    hostv1alpha1.HostInternalDNS,
			Address: bmh.Status.HardwareDetails.Hostname,
		})
	}

	return addrs
}

// GetBaremetalHostID return the provider identifier for this machine when the
// host is provisionned otherwise return nil or an error.
func (m *HostManager) GetBaremetalHostID(ctx context.Context) (*string, error) {
	// look for associated BMH
	bmh, _, err := m.getBmh(ctx)
	if err != nil {
		m.SetError("Failed to get a BaremetalHost for the Host",
			hostv1alpha1.CreateHostError,
		)
		return nil, err
	}
	if bmh == nil {
		m.Log.Info("BaremetalHost not associated, requeuing")
		return nil, &RequeueAfterError{RequeueAfter: requeueAfter}
	}
	if bmh.Status.Provisioning.State == bmov1alpha1.StateProvisioned {
		bmhID := string(bmh.ObjectMeta.UID)
		m.Host.Status.HostUID = bmhID
		m.Host.Status.Ready = true
		return pointer.String(bmhID), nil
	} else {
		m.Host.Status.Ready = false
	}
	m.Log.Info("Provisioning BaremetalHost, requeuing")
	// Do not requeue since BMH update will trigger a reconciliation
	return nil, nil
}

// HasAnnotation makes sure the host has an annotation that references a host.
func (m *HostManager) HasAnnotation() bool {
	annotations := m.Host.ObjectMeta.GetAnnotations()
	if annotations == nil {
		return false
	}
	_, ok := annotations[HostAnnotation]
	return ok
}

// Delete deletes a metal3 machine and is invoked by the Machine Controller.
func (m *HostManager) Delete(ctx context.Context) error {
	m.Log.Info("Deleting host machine", "host", m.Host.Name)

	// clear an error if one was previously set.
	m.clearError()

	if Capm3FastTrack == "" {
		Capm3FastTrack = "false"
		m.Log.Info("Capm3FastTrack is not set, setting it to default value false")
	}

	bmh, helper, err := m.getBmh(ctx)
	if err != nil {
		return err
	}
	if bmh == nil {
		m.Log.Info("baremetalhost not found for host", "host", m.Host.Name)
		return nil
	}

	if bmh.Spec.ConsumerRef != nil {
		// don't remove the ConsumerRef if it references some other  metal3 machine
		if !consumerRefMatches(bmh.Spec.ConsumerRef, m.Host) {
			m.Log.Info("host already associated with another metal3 machine",
				"host", bmh.Name)
			return nil
		}

		bmhUpdated := false

		if bmh.Spec.Image != nil {
			bmh.Spec.Image = nil
			bmhUpdated = true
		}
		if m.Host.Spec.UserData != nil && bmh.Spec.UserData != nil {
			bmh.Spec.UserData = nil
			bmhUpdated = true
		}
		if m.Host.Spec.MetaData != nil && bmh.Spec.MetaData != nil {
			bmh.Spec.MetaData = nil
			bmhUpdated = true
		}
		if m.Host.Spec.NetworkData != nil && bmh.Spec.NetworkData != nil {
			bmh.Spec.NetworkData = nil
			bmhUpdated = true
		}

		//	Change bmh's online status to on/off  based on AutomatedCleaningMode and Capm3FastTrack values
		//	AutomatedCleaningMode |	Capm3FastTrack|   BMH
		//		disabled				false 			turn off
		//		disabled				true 			turn off
		//		metadata				false 			turn off
		//		metadata				true 			turn on

		onlineStatus := bmh.Spec.Online

		if bmh.Spec.AutomatedCleaningMode == "disabled" {
			bmh.Spec.Online = false
		} else if Capm3FastTrack == "true" {
			bmh.Spec.Online = true
		} else if Capm3FastTrack == "false" {
			bmh.Spec.Online = false
		}
		m.Log.Info("Set host Online field by AutomatedCleaningMode",
			"host", bmh.Name,
			"automatedCleaningMode", bmh.Spec.AutomatedCleaningMode,
			"hostSpecOnline", bmh.Spec.Online)

		if onlineStatus != bmh.Spec.Online {
			bmhUpdated = true
		}

		if bmhUpdated {
			// Update the BMH object, if the errors are NotFound, do not return the
			// errors.
			if err := patchIfFound(ctx, helper, bmh); err != nil {
				return err
			}

			m.Log.Info("Deprovisioning BaremetalHost, requeuing")
			return &RequeueAfterError{}
		}

		waiting := true
		switch bmh.Status.Provisioning.State {
		case bmov1alpha1.StateRegistering,
			bmov1alpha1.StateMatchProfile, bmov1alpha1.StateInspecting,
			bmov1alpha1.StateReady, bmov1alpha1.StateAvailable, bmov1alpha1.StateNone,
			bmov1alpha1.StateUnmanaged:
			// Host is not provisioned.
			waiting = false
		case bmov1alpha1.StateExternallyProvisioned:
			// We have no control over provisioning, so just wait until the
			// host is powered off.
			waiting = bmh.Status.PoweredOn
		}
		if waiting {
			m.Log.Info("Deprovisioning BaremetalHost, requeuing")
			return &RequeueAfterError{RequeueAfter: requeueAfter}
		}

		bmh.Spec.ConsumerRef = nil

		m.Log.Info("Removing Paused Annotation (if any)")
		if bmh.Annotations != nil && bmh.Annotations[bmov1alpha1.PausedAnnotation] == PausedAnnotationKey {
			delete(bmh.Annotations, bmov1alpha1.PausedAnnotation)
		}

		// Update the BMH object, if the errors are NotFound, do not return the
		// errors.
		if err := patchIfFound(ctx, helper, bmh); err != nil {
			return err
		}
	}

	m.Log.Info("finished deleting metal3 machine")
	return nil
}

// consumerRefMatches returns a boolean based on whether the consumer
// reference and bare metal machine metadata match.
func consumerRefMatches(consumer *corev1.ObjectReference, host *hostv1alpha1.Host) bool {
	if consumer.Name != host.Name {
		return false
	}
	if consumer.Namespace != host.Namespace {
		return false
	}
	if consumer.Kind != host.Kind {
		return false
	}
	if consumer.GroupVersionKind().Group != host.GroupVersionKind().Group {
		return false
	}
	return true
}

// chooseBMH iterates through known bare-metal hosts and returns one that can be
// associated with the metal3 machine. It searches all hosts in case one already has an
// association with this metal3 machine.
func (m *HostManager) chooseBMH(ctx context.Context) (*bmov1alpha1.BareMetalHost, *patch.Helper, error) {
	// get list of BMH.
	hosts := bmov1alpha1.BareMetalHostList{}
	// Different from M3M: We do not restrict to a namespace
	err := m.client.List(ctx, &hosts)
	if err != nil {
		return nil, nil, err
	}

	// Using the label selector on ListOptions above doesn't seem to work.
	// I think it's because we have a local cache of all BareMetalHosts.
	labelSelector := labels.NewSelector()
	var reqs labels.Requirements

	for labelKey, labelVal := range m.Host.Spec.HostSelector.MatchLabels {
		if matchHostDomainRe.MatchString(labelKey) {
			continue
		}
		m.Log.Info("Adding requirement to match label",
			"label key", labelKey,
			"label value", labelVal)
		r, err := labels.NewRequirement(labelKey, selection.Equals, []string{labelVal})
		if err != nil {
			m.Log.Error(err, "Failed to create MatchLabel requirement, not choosing host")
			return nil, nil, err
		}
		reqs = append(reqs, *r)
	}
	for _, req := range m.Host.Spec.HostSelector.MatchExpressions {
		m.Log.Info("Adding requirement to match label",
			"label key", req.Key,
			"label operator", req.Operator,
			"label value", req.Values)
		lowercaseOperator := selection.Operator(strings.ToLower(string(req.Operator)))
		r, err := labels.NewRequirement(req.Key, lowercaseOperator, req.Values)
		if err != nil {
			m.Log.Error(err, "Failed to create MatchExpression requirement, not choosing host")
			return nil, nil, err
		}
		reqs = append(reqs, *r)
	}
	labelSelector = labelSelector.Add(reqs...)

	availableHosts := []*bmov1alpha1.BareMetalHost{}
	availableHostsWithNodeReuse := []*bmov1alpha1.BareMetalHost{}

	for i, host := range hosts.Items {
		host := host
		if host.Spec.ConsumerRef != nil && consumerRefMatches(host.Spec.ConsumerRef, m.Host) {
			m.Log.Info("Found host with existing ConsumerRef", "host", host.Name)
			helper, err := patch.NewHelper(&hosts.Items[i], m.client)
			return &hosts.Items[i], helper, err
		}
		if host.Spec.ConsumerRef != nil { /* ||
			(m.nodeReuseLabelExists(ctx, &host) &&
				!m.nodeReuseLabelMatches(ctx, &host)) { */
			continue
		}
		if host.GetDeletionTimestamp() != nil {
			continue
		}
		if host.Status.ErrorMessage != "" {
			continue
		}

		// continue if BaremetalHost is paused or marked with UnhealthyAnnotation.
		annotations := host.GetAnnotations()
		if annotations != nil {
			if _, ok := annotations[bmov1alpha1.PausedAnnotation]; ok {
				continue
			}
			if _, ok := annotations[UnhealthyAnnotation]; ok {
				continue
			}
		}

		if labelSelector.Matches(labels.Set(host.ObjectMeta.Labels)) {
			/*
				if m.nodeReuseLabelExists(ctx, &host) && m.nodeReuseLabelMatches(ctx, &host) {
					m.Log.Info("Found host with nodeReuseLabelName and it matches, adding it to availableHostsWithNodeReuse list", "host", host.Name)
					availableHostsWithNodeReuse = append(availableHostsWithNodeReuse, &hosts.Items[i])
				} else if !m.nodeReuseLabelExists(ctx, &host) {
			*/
			switch host.Status.Provisioning.State {
			case bmov1alpha1.StateReady, bmov1alpha1.StateAvailable:
			default:
				continue
			}
			m.Log.Info("Host matched hostSelector for Host, adding it to availableHosts list", "host", host.Name)
			availableHosts = append(availableHosts, &hosts.Items[i])
			//			}
		} else {
			m.Log.Info("Host did not match hostSelector for Host", "host", host.Name)
		}
	}

	m.Log.Info("Host count available with nodeReuseLabelName while choosing host for Metal3 machine", "hostcount", len(availableHostsWithNodeReuse))
	m.Log.Info("Host count available while choosing host for Metal3 machine", "hostcount", len(availableHosts))
	if len(availableHostsWithNodeReuse) == 0 && len(availableHosts) == 0 {
		return nil, nil, nil
	}

	// choose a host.
	var chosenHost *bmov1alpha1.BareMetalHost

	// If there are hosts with nodeReuseLabelName:
	if len(availableHostsWithNodeReuse) != 0 {
		for _, host := range availableHostsWithNodeReuse {
			// Build list of hosts in Ready state with nodeReuseLabelName
			hostsInAvailableStateWithNodeReuse := []*bmov1alpha1.BareMetalHost{}
			// Build list of hosts in any other state than Ready state with nodeReuseLabelName
			hostsInNotAvailableStateWithNodeReuse := []*bmov1alpha1.BareMetalHost{}
			if host.Status.Provisioning.State == bmov1alpha1.StateReady || host.Status.Provisioning.State == bmov1alpha1.StateAvailable {
				hostsInAvailableStateWithNodeReuse = append(hostsInAvailableStateWithNodeReuse, host)
			} else {
				hostsInNotAvailableStateWithNodeReuse = append(hostsInNotAvailableStateWithNodeReuse, host)
			}

			// If host is found in `Ready` state, pick it
			if len(hostsInAvailableStateWithNodeReuse) != 0 {
				m.Log.Info("Found host(s) with nodeReuseLabelName in Ready/Available state, choosing the host", "availabeHostCount", len(hostsInAvailableStateWithNodeReuse), "host", host.Name)
				rHost, _ := rand.Int(rand.Reader, big.NewInt(int64(len(hostsInAvailableStateWithNodeReuse))))
				randomHost := rHost.Int64()
				chosenHost = hostsInAvailableStateWithNodeReuse[randomHost]
			} else if len(hostsInNotAvailableStateWithNodeReuse) != 0 {
				m.Log.Info("Found host(s) with nodeReuseLabelName in not-available state, requeuing the host", "notAvailabeHostCount", len(hostsInNotAvailableStateWithNodeReuse), "hoststate", host.Status.Provisioning.State, "host", host.Name)
				return nil, nil, &RequeueAfterError{RequeueAfter: requeueAfter}
			}
		}
	} else {
		// If there are no hosts with nodeReuseLabelName, fall back
		// to the current flow and select hosts randomly.
		m.Log.Info("host(s) count available, choosing a random host", "availabeHostCount", len(availableHosts))
		rHost, _ := rand.Int(rand.Reader, big.NewInt(int64(len(availableHosts))))
		randomHost := rHost.Int64()
		chosenHost = availableHosts[randomHost]
	}

	helper, err := patch.NewHelper(chosenHost, m.client)
	return chosenHost, helper, err
}

// setHostConsumerRef will ensure the host's Spec is set to link to this
// Host.
func (m *HostManager) setHostConsumerRef(bmh *bmov1alpha1.BareMetalHost) error {
	bmh.Spec.ConsumerRef = &corev1.ObjectReference{
		Kind:       "Host",
		Name:       m.Host.Name,
		Namespace:  m.Host.Namespace,
		APIVersion: m.Host.APIVersion,
	}

	/*
		// Delete nodeReuseLabelName from host.
		m.Log.Info("Deleting nodeReuseLabelName from host, if any")

		labels := host.GetLabels()
		if labels != nil {
			if _, ok := labels[nodeReuseLabelName]; ok {
				delete(host.Labels, nodeReuseLabelName)
				m.Log.Info("Finished deleting nodeReuseLabelName")
			}
		}
	*/
	return nil
}

func (m *HostManager) syncInfo(bmh *bmov1alpha1.BareMetalHost) {
	// synchronize BooMACAddress
	m.Host.Status.BootMACAddress = bmh.Spec.BootMACAddress
	// synchronize NICs
	nics := make([]hostv1alpha1.NIC, len(bmh.Status.HardwareDetails.NIC))
	for i, nic := range bmh.Status.HardwareDetails.NIC {
		nics[i] = hostv1alpha1.NIC{
			Name: nic.Name, MAC: nic.MAC, IP: nic.IP, PXE: nic.PXE,
		}
	}
	m.Host.Status.NIC = nics
	// Assign on empty maps will not work
	if bmh.Annotations == nil {
		bmh.Annotations = map[string]string{}
	}
	if bmh.Labels == nil {
		bmh.Labels = map[string]string{}
	}
	if m.Host.Annotations == nil {
		m.Host.Annotations = map[string]string{}
	}
	if m.Host.Labels == nil {
		m.Host.Labels = map[string]string{}
	}
	// synchronize labels (both ways)
	m.Host.Annotations[hostv1alpha1.SyncedLabelsAnnotation] = syncMaps(
		m.Host.Labels, bmh.Labels,
		m.Host.Annotations[hostv1alpha1.SyncedLabelsAnnotation])
	// synchronize annotations (both ways)
	m.Host.Annotations[hostv1alpha1.SyncedAnnotationsAnnotation] = syncMaps(
		m.Host.Annotations, bmh.Annotations,
		m.Host.Annotations[hostv1alpha1.SyncedAnnotationsAnnotation])
}

// Transfer elements from argument bmhMap to hostMap and deletes the elements from
// hostMap whose key is in synced and do not appear in bmhMap. The result of the
// function is the comma separated names of keys from bmhMap.
func syncMaps(hostMap, bmhMap map[string]string, synced string) string {
	var keys []string
	if synced != "" {
		keys = strings.Split(synced, ",")
	}
	for _, key := range keys {
		_, ok := bmhMap[key]
		if !ok {
			delete(hostMap, key)
		}
	}
	keys = nil
	for key, v := range bmhMap {
		elts := strings.Split(key, "/")
		if len(elts) == 2 {
			domain := elts[0]
			if domain == "kubernetes.io" || domain == hostv1alpha1.HostDomain ||
				strings.HasSuffix(domain, ".kubernetes.io") {
				continue
			}
		}
		keys = append(keys, key)
		hostMap[key] = v
	}
	return strings.Join(keys, ",")
}
