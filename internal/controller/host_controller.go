/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"time"

	bmov1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	"github.com/pkg/errors"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-operator/api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/cluster-api/util/conditions"
	"sigs.k8s.io/cluster-api/util/patch"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logr "sigs.k8s.io/controller-runtime/pkg/log"

	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)

const (
	BmhAnnotation = "metal3.io/BareMetalHost"
	requeueAfter  = time.Second * 30
)

var hasRequeueAfterError HasRequeueAfterError

// HostReconciler reconciles a Host object
type HostReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=kanod.io,resources=hosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kanod.io,resources=hosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=kanod.io,resources=hosts/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch
// +kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;update;patch
//+kubebuilder:rbac:groups=kanod.io,resources=hostquotas,verbs=get;list;watch
//+kubebuilder:rbac:groups=kanod.io,resources=hostquotas/status,verbs=get;update;patch
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Host object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *HostReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logr.FromContext(ctx).WithName("host-controller").WithValues("host", req.NamespacedName)
	host := &hostv1alpha1.Host{}
	if err := r.Client.Get(ctx, req.NamespacedName, host); err != nil {
		if apierrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if host.Spec.Kind != "baremetal" {
		// Not handled by this controller.
		log.Info("Skipping not associated to baremetal. Bad Label association")
		return ctrl.Result{}, nil
	}
	patchHelper, err := patch.NewHelper(host, r.Client)
	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "failed to init patch helper")
	}
	defer func() {
		if err := patchHost(ctx, patchHelper, host); err != nil {
			log.Error(err, "failed to Patch metal3Machine")
		}
	}()
	// clear an error if one was previously set
	clearErrorHost(host)

	// Create a helper for managing the baremetal container hosting the machine.
	hostMgr, err := NewHostManager(r.Client, log, host)
	if err != nil {
		return ctrl.Result{}, errors.Wrapf(err, "failed to create helper for managing the hostMgr")
	}

	if HasAnnotation(host, hostv1alpha1.PausedAnnotation) {
		// set pause annotation on associated bmh (if any)
		err := hostMgr.SetPauseAnnotation(ctx)
		if err != nil {
			log.Info("failed to set pause annotation on associated bmh")
			conditions.MarkFalse(
				host, hostv1alpha1.AssociateBMHCondition,
				hostv1alpha1.PauseAnnotationSetFailedReason,
				clusterv1.ConditionSeverityInfo, "")
			return ctrl.Result{}, nil
		}
		conditions.MarkFalse(
			host, hostv1alpha1.AssociateBMHCondition,
			hostv1alpha1.HostPausedReason,
			clusterv1.ConditionSeverityInfo, "")
		return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, nil
	} else {
		err := hostMgr.RemovePauseAnnotation(ctx)
		if err != nil {
			log.Info("failed to check pause annotation on associated bmh")
			conditions.MarkFalse(
				host, hostv1alpha1.AssociateBMHCondition,
				hostv1alpha1.PauseAnnotationRemoveFailedReason,
				clusterv1.ConditionSeverityInfo, "")
			return ctrl.Result{}, nil
		}
	}
	// Handle deleted machines
	if !host.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(ctx, hostMgr)
	}

	// Handle non-deleted machines
	return r.reconcileNormal(ctx, hostMgr)
}

func (r *HostReconciler) reconcileNormal(ctx context.Context,
	hostMgr HostManagerInterface,
) (ctrl.Result, error) {
	// If the Host doesn't have finalizer, add it.
	hostMgr.SetFinalizer()

	// if the machine is already provisioned, update and return
	if hostMgr.IsProvisioned() {
		errType := hostv1alpha1.UpdateHostError
		return checkMachineError(hostMgr, hostMgr.Update(ctx),
			"Failed to update the Host", errType,
		)
	}

	errType := hostv1alpha1.CreateHostError

	// Check if the metal3machine was associated with a baremetalhost
	if !hostMgr.HasAnnotation() {
		// Associate the baremetalhost hosting the machine
		err := hostMgr.Associate(ctx)
		if err != nil {
			hostMgr.SetConditionHostToFalse(hostv1alpha1.AssociateBMHCondition, hostv1alpha1.AssociateBMHFailedReason, clusterv1.ConditionSeverityError, err.Error())
			return checkMachineError(hostMgr, err,
				"failed to associate the Host to a BaremetalHost", errType,
			)
		}
	}
	// Update Condition to reflect that we have an associated BMH
	hostMgr.SetConditionHostToTrue(hostv1alpha1.AssociateBMHCondition)

	err := hostMgr.Update(ctx)
	if err != nil {
		return checkMachineError(hostMgr, err,
			"failed to update BaremetalHost", errType,
		)
	}

	_, err = hostMgr.GetBaremetalHostID(ctx)
	if err != nil {
		hostMgr.SetConditionHostToFalse(hostv1alpha1.AssociateBMHCondition, hostv1alpha1.MissingBMHReason, clusterv1.ConditionSeverityError, err.Error())
		return checkMachineError(hostMgr, err,
			"failed to get the providerID for the metal3machine", errType,
		)
	}
	return ctrl.Result{}, err
}

func (r *HostReconciler) reconcileDelete(ctx context.Context,
	hostMgr HostManagerInterface,
) (ctrl.Result, error) {
	// set machine condition to Deleting
	hostMgr.SetConditionHostToFalse(hostv1alpha1.AssociateBMHCondition, hostv1alpha1.DeletingReason, clusterv1.ConditionSeverityInfo, "")

	errType := hostv1alpha1.DeleteHostError

	// delete the machine
	if err := hostMgr.Delete(ctx); err != nil {
		hostMgr.SetConditionHostToFalse(hostv1alpha1.AssociateBMHCondition, hostv1alpha1.DeletionFailedReason, clusterv1.ConditionSeverityWarning, err.Error())
		return checkMachineError(hostMgr, err,
			"failed to delete Host", errType,
		)
	}

	// metal3machine is marked for deletion and ready to be deleted,
	// so remove the finalizer.
	hostMgr.UnsetFinalizer()

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *HostReconciler) SetupWithManager(mgr ctrl.Manager) error {
	// This is still applied on client side not server side.
	return ctrl.NewControllerManagedBy(mgr).
		For(&hostv1alpha1.Host{}).
		Watches(
			&bmov1alpha1.BareMetalHost{},
			handler.EnqueueRequestsFromMapFunc(r.BareMetalHostToMetal3Machines),
		).
		Complete(r)
}

// BareMetalHostToMetal3Machines will return a reconcile request for a Metal3Machine if the event is for a
// BareMetalHost and that BareMetalHost references a Metal3Machine.
func (r *HostReconciler) BareMetalHostToMetal3Machines(_ context.Context, obj client.Object) []ctrl.Request {
	if host, ok := obj.(*bmov1alpha1.BareMetalHost); ok {
		if host.Spec.ConsumerRef != nil &&
			host.Spec.ConsumerRef.Kind == "Host" &&
			host.Spec.ConsumerRef.GroupVersionKind().Group == hostv1alpha1.GroupVersion.Group {
			return []ctrl.Request{
				{
					NamespacedName: types.NamespacedName{
						Name:      host.Spec.ConsumerRef.Name,
						Namespace: host.Spec.ConsumerRef.Namespace,
					},
				},
			}
		}
	} else {
		log := logr.Log.WithName("host-controller")
		log.Error(errors.Errorf("expected a BareMetalHost but got a %T", obj),
			"failed to get Host for BareMetalHost",
		)
	}
	return []ctrl.Request{}
}

func patchHost(ctx context.Context, patchHelper *patch.Helper, host *hostv1alpha1.Host, options ...patch.Option) error {
	// Always update the readyCondition by summarizing the state of other conditions.
	conditions.SetSummary(host,
		conditions.WithConditions(
			hostv1alpha1.AssociateBMHCondition,
		),
	)

	// Patch the object, ignoring conflicts on the conditions owned by this controller.
	options = append(
		options,
		patch.WithOwnedConditions{Conditions: []clusterv1.ConditionType{
			clusterv1.ReadyCondition,
			hostv1alpha1.AssociateBMHCondition,
		}},
		patch.WithStatusObservedGeneration{},
	)
	return patchHelper.Patch(ctx, host, options...)
}

// clearError removes the ErrorMessage from the metal3machine's Status if set.
func clearErrorHost(host *hostv1alpha1.Host) {
	if host.Status.FailureMessage != nil || host.Status.FailureReason != nil {
		host.Status.FailureMessage = nil
		host.Status.FailureReason = nil
	}
}

func checkMachineError(hostMgr HostManagerInterface, err error,
	errMessage string, errType hostv1alpha1.HostStatusError,
) (ctrl.Result, error) {
	if err == nil {
		return ctrl.Result{}, nil
	}
	if ok := errors.As(err, &hasRequeueAfterError); ok {
		return ctrl.Result{Requeue: true, RequeueAfter: hasRequeueAfterError.GetRequeueAfter()}, nil
	}
	hostMgr.SetError(errMessage, errType)
	return ctrl.Result{}, errors.Wrap(err, errMessage)
}
