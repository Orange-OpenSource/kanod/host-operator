/*
Copyright 2024 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/selection"
	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)

const (
	HostDomain                  = "host.kanod.io"
	HostFinalizer               = "host.kanod.io"
	PausedAnnotation            = "kanod.io/paused"
	SyncedLabelsAnnotation      = "kanod.io/synced-labels"
	SyncedAnnotationsAnnotation = "kanod.io/synced-annotations"
	HostKindLabel               = "host.kanod.io/kind"
)

const (
	// AssociateBMHCondition documents the status of associated the Host with a BaremetalHost.
	AssociateBMHCondition clusterv1.ConditionType = "AssociateBMH"

	// WaitingForClusterInfrastructureReason used when waiting for cluster
	// infrastructure to be ready before proceeding.
	// WaitingForClusterInfrastructureReason = "WaitingForClusterInfrastructure"
	// WaitingForBootstrapReadyReason used when waiting for bootstrap to be ready before proceeding.
	// WaitingForBootstrapReadyReason = "WaitingForBootstrapReady"
	// MissingBMHReason used when BMH is not yet accessible
	MissingBMHReason = "MissingBMH"
	// AssociateBMHFailedReason documents any errors while associating Host with a BaremetalHost.
	AssociateBMHFailedReason = "AssociateBMHFailed"
	// WaitingForMetal3MachineOwnerRefReason is used when Metal3Machine is waiting for OwnerReference to be
	// set before proceeding.
	// WaitingForMetal3MachineOwnerRefReason = "WaitingForM3MachineOwnerRef"
	// WaitingforMetal3ClusterReason is used when Metal3Machine is waiting for Metal3Cluster.
	// WaitingforMetal3ClusterReason = "WaitingforMetal3Cluster"

	// HostPausedReason is used when Host or Cluster is paused.
	HostPausedReason = "HostPaused"
	// PauseAnnotationRemoveFailedReason is used when failed to remove/check pause annotation on associated bmh.
	PauseAnnotationRemoveFailedReason = "PauseAnnotationRemoveFailed"
	// PauseAnnotationSetFailedReason is used when failed to set pause annotation on associated bmh.
	PauseAnnotationSetFailedReason = "PauseAnnotationSetFailed"
	// DeletingReason
	DeletingReason = "Deleting"
	// DeletionFailedReason
	DeletionFailedReason = "DeletionFailed"
)

// Image holds the details of an image either to provisioned or that
// has been provisioned.
type Image struct {
	// URL is a location of an image to deploy.
	URL string `json:"url"`

	// Checksum is the checksum for the image.
	Checksum string `json:"checksum,omitempty"`

	// ChecksumType is the checksum algorithm for the image, e.g md5, sha256 or sha512.
	// The special value "auto" can be used to detect the algorithm from the checksum.
	// If missing, MD5 is used. If in doubt, use "auto".
	// +kubebuilder:validation:Enum=md5;sha256;sha512;auto
	// +optional
	ChecksumType *string `json:"checksumType,omitempty"`

	// DiskFormat contains the format of the image (raw, qcow2, ...).
	// Needs to be set to raw for raw images streaming.
	// Note live-iso means an iso referenced by the url will be live-booted
	// and not deployed to disk, and in this case the checksum options
	// are not required and if specified will be ignored.
	// +kubebuilder:validation:Enum=raw;qcow2;vdi;vmdk;live-iso
	// +optional
	DiskFormat *string `json:"format,omitempty"`
}

// HostSelector specifies matching criteria for labels on BareMetalHosts.
// This is used to limit the set of BareMetalHost objects considered for
// claiming for a Machine.
type HostSelector struct {
	// Key/value pairs of labels that must exist on a chosen BareMetalHost
	// +optional
	MatchLabels map[string]string `json:"matchLabels,omitempty"`

	// Label match expressions that must be true on a chosen BareMetalHost
	// +optional
	MatchExpressions []HostSelectorRequirement `json:"matchExpressions,omitempty"`
}

type HostSelectorRequirement struct {
	Key      string             `json:"key"`
	Operator selection.Operator `json:"operator"`
	Values   []string           `json:"values"`
}

type HostStatusError string

const (
	CreateHostError HostStatusError = "CreateError"
	DeleteHostError HostStatusError = "DeleteError"
	UpdateHostError HostStatusError = "UpdateError"
)

// HostSpec defines the desired state of Host
type HostSpec struct {
	// Kind identifies the target of the host. If not set will default
	// to host.kanod.io/kind selector
	Kind string `json:"kind,omitempty"`

	// Should the server be online?
	Online bool `json:"online"`

	// ConsumerRef can be used to store information about something
	// that is using a host. When it is not empty, the host is
	// considered "in use".
	ConsumerRef *corev1.ObjectReference `json:"consumerRef,omitempty"`

	// Image holds the details of the image to be provisioned.
	Image *Image `json:"image,omitempty"`

	// UserData holds the reference to the Secret containing the user
	// data to be passed to the host before it boots.
	UserData *corev1.SecretReference `json:"userData,omitempty"`

	// NetworkData holds the reference to the Secret containing network
	// configuration (e.g content of network_data.json) which is passed
	// to the Config Drive.
	NetworkData *corev1.SecretReference `json:"networkData,omitempty"`

	// MetaData holds the reference to the Secret containing host metadata
	// (e.g. meta_data.json) which is passed to the Config Drive.
	MetaData *corev1.SecretReference `json:"metaData,omitempty"`

	// CPU architecture of the host, e.g. "x86_64" or "aarch64". If set will restrict
	// binding to bareMetalHost with the same value.
	// +optional
	Architecture string `json:"architecture,omitempty"`

	// HostSelector specifies matching criteria for labels on BareMetalHosts.
	// This is used to limit the set of BareMetalHost objects considered for
	// claiming for a metal3machine.
	// +optional
	HostSelector HostSelector `json:"hostSelector,omitempty"`
}

// ProvisionState describe synthetically the provisioning state of the
// underlying resource.
type ProvisionState string

// HostAddressType describes a valid HostAddress type.
type HostAddressType string

// Define the HostAddressType constants.
const (
	HostHostName    HostAddressType = "Hostname"
	HostExternalIP  HostAddressType = "ExternalIP"
	HostInternalIP  HostAddressType = "InternalIP"
	HostExternalDNS HostAddressType = "ExternalDNS"
	HostInternalDNS HostAddressType = "InternalDNS"
)

// HostAddress contains information for the node's address.
type HostAddress struct {
	// Host address type, one of Hostname, ExternalIP, InternalIP, ExternalDNS or InternalDNS.
	Type HostAddressType `json:"type"`

	// The machine address.
	Address string `json:"address"`
}

type NIC struct {
	// The name of the network interface, e.g. "en0"
	Name string `json:"name"`
	// The device MAC address
	MAC string `json:"MAC"`
	// The IP address of the interface. This will be an IPv4 or IPv6 address
	// if one is present.  If both IPv4 and IPv6 addresses are present in a
	// dual-stack environment, two nics will be output, one with each IP.
	IP string `json:"ip,omitempty"`
	// Whether the NIC is PXE Bootable
	PXE bool `json:"pxe,omitempty"`
}

// HostAddresses is a slice of HostAddress items to be used by infrastructure providers.
type HostAddresses []HostAddress

// HostStatus defines the observed state of Host
type HostStatus struct {
	// LastUpdated identifies when this status was last observed.
	// +optional
	LastUpdated *metav1.Time `json:"lastUpdated,omitempty"`
	// Conditions defines current service state of the Host.
	// +optional
	Conditions clusterv1.Conditions `json:"conditions,omitempty"`
	// FailureReason will be set in the event that there is a terminal problem
	// reconciling the host and will contain a succinct value suitable
	// for machine interpretation.
	//
	// This field should not be set for transitive errors that a controller
	// faces that are expected to be fixed automatically over
	// time (like service outages), but instead indicate that something is
	// fundamentally wrong with the host's spec or the configuration of
	// the controller, and that manual intervention is required.
	//
	// Any transient errors that occur during the reconciliation of
	// host can be added as events to the host object
	// and/or logged in the controller's output.
	// +optional
	FailureReason *HostStatusError `json:"failureReason,omitempty"`

	// FailureMessage will be set in the event that there is a terminal problem
	// reconciling the host and will contain a more verbose string suitable
	// for logging and human consumption.
	// +optional
	FailureMessage *string `json:"failureMessage,omitempty"`

	// Synced means the metaData of the host is synced with the compute resource
	Synced bool `json:"synced,omitempty"`
	// Ready is the state of the host
	// It means the reconciliation completed.
	Ready bool `json:"ready,omitempty"`
	// Addresses is a list of addresses assigned to the machine.
	// This field is copied from the infrastructure provider reference.
	// +optional
	Addresses HostAddresses `json:"addresses,omitempty"`
	// HostUID is the UID of the underlying compute. It is used
	// as ID by the capm3 controller.
	// +optional
	HostUID string `json:"hostUID,omitempty"`
	// The network interfaces on the underlying compute resource
	NIC []NIC `json:"nics,omitempty"`
	// The MACAddress of the interface used to boot the underlying compute resource
	BootMACAddress string `json:"bootMACAddress,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Ready",type="string",JSONPath=".status.ready",description="Whether the host is ready or not"
// +kubebuilder:printcolumn:name="Online",type="string",JSONPath=".spec.online",description="Whether the host is online or not"
// Host is the Schema for the hosts API
type Host struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   HostSpec   `json:"spec,omitempty"`
	Status HostStatus `json:"status,omitempty"`
}

// Implementation of cluster-api ConditionGetter and ConditionSetter

// GetConditions returns the list of conditions for an Host API object.
func (c *Host) GetConditions() clusterv1.Conditions {
	return c.Status.Conditions
}

// SetConditions will set the given conditions on an Host object.
func (c *Host) SetConditions(conditions clusterv1.Conditions) {
	c.Status.Conditions = conditions
}

//+kubebuilder:object:root=true

// HostList contains a list of Host
type HostList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Host `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Host{}, &HostList{})
}
