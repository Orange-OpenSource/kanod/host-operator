/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"context"
	"fmt"
	"reflect"
	"strconv"

	quotav1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-quota/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// log is for logging in this package.
var (
	hostlog              = logf.Log.WithName("host-resource")
	webhookClient        client.Client
	webhookEventRecorder record.EventRecorder
)

func (r *Host) SetupWebhookWithManager(mgr ctrl.Manager) error {
	webhookClient = mgr.GetClient()
	webhookEventRecorder = mgr.GetEventRecorderFor("host-resource")

	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch
//+kubebuilder:webhook:path=/mutate-kanod-io-v1alpha1-host,mutating=true,failurePolicy=fail,sideEffects=None,groups=kanod.io,resources=hosts,verbs=create;update,versions=v1alpha1,name=mhost.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &Host{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *Host) Default() {
	hostlog.Info("Apply default webhook", "name", r.Name)

	// TODO(user): fill in your defaulting logic.
	if _, ok := r.Spec.HostSelector.MatchLabels[HostKindLabel]; ok && r.Spec.Kind == "" {
		r.Spec.Kind = r.Spec.HostSelector.MatchLabels[HostKindLabel]
	}
	if r.Labels == nil {
		r.Labels = make(map[string]string, 0)
	}
	r.Labels[HostKindLabel] = r.Spec.Kind
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-kanod-io-v1alpha1-host,mutating=false,failurePolicy=fail,sideEffects=None,groups=kanod.io,resources=hosts,verbs=create;update,versions=v1alpha1,name=vhost.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &Host{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Host) ValidateCreate() (admission.Warnings, error) {
	hostlog.Info("Webhook: validate create", "name", r.Name)
	err := r.checkQuotas(3)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Host) ValidateUpdate(old runtime.Object) (admission.Warnings, error) {
	return nil, nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Host) ValidateDelete() (admission.Warnings, error) {
	return nil, nil
}

func (r *Host) checkQuotas(numRetry int) error {
	var err error
	retry := false

	resourcesQuota, err := r.getResourcesQuota()
	if err != nil {
		hostlog.Error(err, "Failed to list HostQuota")
		return err
	}

	hostsList, err := r.getHostsInNamespace(r.Namespace)
	if err != nil {
		hostlog.Error(err, "Failed to list HostQuota")
		return err
	}

	for _, hostQuota := range resourcesQuota {
		quotaOk, err := r.matchQuotaResource(hostQuota, hostsList, r.Spec.HostSelector.MatchLabels)
		if err != nil {
			return err
		}
		if !quotaOk {
			return fmt.Errorf("quota exceeded for hostquota %s", hostQuota.Name)
		}
	}

	for _, hostQuota := range resourcesQuota {
		err := r.updateCountInHostQuotaStatus(hostQuota, hostsList, r.Spec.HostSelector.MatchLabels)
		if err != nil {
			retry = true
			break
		}
	}

	if retry && numRetry > 0 {
		r.checkQuotas(numRetry - 1)
	}

	return err
}

func (r *Host) matchQuotaResource(
	hostQuota quotav1alpha1.HostQuota,
	hostsList []Host,
	hostMatchLabels map[string]string,
) (bool, error) {
	usedQuotasList, err := getQuotaQuantity(hostQuota, hostsList)
	if err != nil {
		return false, err
	}

	for i := range hostQuota.Spec.Hard {
		var increment int
		quota := hostQuota.Spec.Hard[i]
		if !quotaLabelMatchHost(hostMatchLabels, quota) {
			continue
		}

		increment, err := computeQuantityIncrement(hostMatchLabels, quota)
		if err != nil {
			hostlog.Error(err, "error during computeQuantityIncrement")
			return false, err
		}

		quotaRequest := usedQuotasList[i] + increment
		if (quotaRequest) > quota.Quantity {
			msg := fmt.Sprintf("quota exceeded for %s:%s %d (capacity: %d)", quota.LabelName, quota.LabelValue, quotaRequest, quota.Quantity)
			hostlog.Info(msg)
			webhookEventRecorder.Event(r, corev1.EventTypeWarning, "host quota exceeded", msg)
			return false, nil
		}

		msg := fmt.Sprintf("quota ok for %s:%s %d (capacity: %d)", quota.LabelName, quota.LabelValue, quotaRequest, quota.Quantity)
		hostlog.Info(msg)
	}
	return true, nil
}

func quotaLabelMatchHost(hostMatchLabels map[string]string, quota quotav1alpha1.Quota) bool {
	for labelSelector := range quota.LabelSelector {
		hostLabelValue, ok := hostMatchLabels[labelSelector]
		if !ok {
			return false
		}
		if hostLabelValue != quota.LabelSelector[labelSelector] {
			return false
		}
	}

	labelValue, ok := hostMatchLabels[quota.LabelName]
	if !ok {
		return false
	}
	if quota.LabelValue != "" && labelValue != quota.LabelValue {
		return false
	}
	return true
}

func computeQuantityIncrement(hostLabels map[string]string, quota quotav1alpha1.Quota) (int, error) {
	var val int
	var err error
	if quota.Aggregate {
		val, err = strconv.Atoi(hostLabels[quota.LabelName])
		if err != nil {
			hostlog.Error(err, "error during string conversion to int", "hostLabels", hostLabels, "quota.LabelName", quota.LabelName)
			return 0, err
		}
	} else {
		val = 1
	}
	return val, err

}
func (r *Host) updateCountInHostQuotaStatus(
	hostQuota quotav1alpha1.HostQuota,
	hostsList []Host,
	hostLabels map[string]string,
) error {
	usedQuotasList, err := getQuotaQuantity(hostQuota, hostsList)
	if err != nil {
		return err
	}
	updateHostQuota := false
	for i := range hostQuota.Spec.Hard {
		var increment int
		quota := hostQuota.Spec.Hard[i]

		if !quotaLabelMatchHost(hostLabels, quota) {
			continue
		}

		increment, err := computeQuantityIncrement(hostLabels, quota)
		if err != nil {
			return err
		}

		usedQuotasList[i] = usedQuotasList[i] + increment
		updateHostQuota = true
	}
	if updateHostQuota {
		err := r.updateHostQuotaStatus(hostQuota, usedQuotasList)
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *Host) updateHostQuotaStatus(
	hostQuota quotav1alpha1.HostQuota,
	usedQuotasList []int,
) error {
	ctx := context.Background()
	currentString := ""
	usedQuotaList := []quotav1alpha1.Quota{}

	for i := range hostQuota.Spec.Hard {
		quota := hostQuota.Spec.Hard[i]
		used := quota.DeepCopy()
		used.Quantity = usedQuotasList[i]
		usedQuotaList = append(usedQuotaList, *used)

		labelNameValue := ""
		if quota.LabelValue != "" {
			labelNameValue = fmt.Sprintf("%s:%s", quota.LabelName, quota.LabelValue)
		} else {
			labelNameValue = quota.LabelName
		}
		count := fmt.Sprintf("%s : %d/%d ", labelNameValue, used.Quantity, quota.Quantity)

		currentString += count
	}

	updatedStatus := quotav1alpha1.HostQuotaStatus{
		Hard:      hostQuota.Spec.Hard,
		Used:      usedQuotaList,
		UsedQuota: currentString,
	}

	if !reflect.DeepEqual(hostQuota.Status, updatedStatus) {
		hostQuota.Status = updatedStatus
		err := webhookClient.Status().Update(ctx, &hostQuota)
		if err != nil {
			hostlog.Error(err, "failed to update hostQuota status", "hostQuota", hostQuota.Name, "status", updatedStatus)
			return err
		}
	}

	return nil
}

func (r *Host) getResourcesQuota() ([]quotav1alpha1.HostQuota, error) {
	hostQuotaList := &quotav1alpha1.HostQuotaList{}
	err := webhookClient.List(context.Background(), hostQuotaList, &client.ListOptions{
		Namespace: r.Namespace,
	})
	if err != nil {
		if apierrors.IsNotFound(err) {
			return nil, nil
		} else {
			hostlog.Error(err, "failed to list HostQuota")
			return nil, err
		}
	}

	return hostQuotaList.Items, nil
}

func (r *Host) getHostsInNamespace(namespace string) ([]Host, error) {
	hostList := &HostList{}
	options := client.ListOptions{
		Namespace: namespace,
	}

	err := webhookClient.List(context.Background(), hostList, &options)
	if err != nil {
		if apierrors.IsNotFound(err) {
			return nil, nil
		} else {
			hostlog.Error(err, "failed to list host")
			return nil, err
		}
	}

	hostlog.Info("host count in namespace", "namespace", namespace, "count", len(hostList.Items))
	return hostList.Items, nil
}

// compute current usage for all quotas in HostQuota
func getQuotaQuantity(hostQuota quotav1alpha1.HostQuota, hostsList []Host) ([]int, error) {
	usedCapacityList := []int{}

	for _, quota := range hostQuota.Spec.Hard {
		usedCapacity, err := ComputeUsedCapacity(quota, hostsList)
		if err != nil {
			return nil, err
		}
		usedCapacityList = append(usedCapacityList, usedCapacity)
	}
	return usedCapacityList, nil
}

// compute current usage for a specific quota label
func ComputeUsedCapacity(
	quota quotav1alpha1.Quota,
	hostList []Host,
) (int, error) {
	usageValue := 0
	for _, host := range hostList {
		hostMatchLabels := host.Spec.HostSelector.MatchLabels
		if hostMatchLabels == nil {
			continue
		}

		if !quotaLabelMatchHost(hostMatchLabels, quota) {
			continue
		}

		if quota.Aggregate {
			val, err := strconv.Atoi(hostMatchLabels[quota.LabelName])
			if err != nil {
				hostlog.Error(err, "error during string conversion to int", "hostMatchLabels", hostMatchLabels, "quota.LabelName", quota.LabelName)
				return 0, err
			}
			usageValue += val
		} else {
			usageValue += 1
		}
	}

	return usageValue, nil
}
