---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.12.0
  name: hosts.kanod.io
spec:
  group: kanod.io
  names:
    kind: Host
    listKind: HostList
    plural: hosts
    singular: host
  scope: Namespaced
  versions:
  - additionalPrinterColumns:
    - description: Whether the host is ready or not
      jsonPath: .status.ready
      name: Ready
      type: string
    - description: Whether the host is online or not
      jsonPath: .spec.online
      name: Online
      type: string
    name: v1alpha1
    schema:
      openAPIV3Schema:
        description: Host is the Schema for the hosts API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: HostSpec defines the desired state of Host
            properties:
              architecture:
                description: CPU architecture of the host, e.g. "x86_64" or "aarch64".
                  If set will restrict binding to bareMetalHost with the same value.
                type: string
              consumerRef:
                description: ConsumerRef can be used to store information about something
                  that is using a host. When it is not empty, the host is considered
                  "in use".
                properties:
                  apiVersion:
                    description: API version of the referent.
                    type: string
                  fieldPath:
                    description: 'If referring to a piece of an object instead of
                      an entire object, this string should contain a valid JSON/Go
                      field access statement, such as desiredState.manifest.containers[2].
                      For example, if the object reference is to a container within
                      a pod, this would take on a value like: "spec.containers{name}"
                      (where "name" refers to the name of the container that triggered
                      the event) or if no container name is specified "spec.containers[2]"
                      (container with index 2 in this pod). This syntax is chosen
                      only to have some well-defined way of referencing a part of
                      an object. TODO: this design is not final and this field is
                      subject to change in the future.'
                    type: string
                  kind:
                    description: 'Kind of the referent. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
                    type: string
                  name:
                    description: 'Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names'
                    type: string
                  namespace:
                    description: 'Namespace of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/'
                    type: string
                  resourceVersion:
                    description: 'Specific resourceVersion to which this reference
                      is made, if any. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#concurrency-control-and-consistency'
                    type: string
                  uid:
                    description: 'UID of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#uids'
                    type: string
                type: object
                x-kubernetes-map-type: atomic
              hostSelector:
                description: HostSelector specifies matching criteria for labels on
                  BareMetalHosts. This is used to limit the set of BareMetalHost objects
                  considered for claiming for a metal3machine.
                properties:
                  matchExpressions:
                    description: Label match expressions that must be true on a chosen
                      BareMetalHost
                    items:
                      properties:
                        key:
                          type: string
                        operator:
                          description: Operator represents a key/field's relationship
                            to value(s). See labels.Requirement and fields.Requirement
                            for more details.
                          type: string
                        values:
                          items:
                            type: string
                          type: array
                      required:
                      - key
                      - operator
                      - values
                      type: object
                    type: array
                  matchLabels:
                    additionalProperties:
                      type: string
                    description: Key/value pairs of labels that must exist on a chosen
                      BareMetalHost
                    type: object
                type: object
              image:
                description: Image holds the details of the image to be provisioned.
                properties:
                  checksum:
                    description: Checksum is the checksum for the image.
                    type: string
                  checksumType:
                    description: ChecksumType is the checksum algorithm for the image,
                      e.g md5, sha256 or sha512. The special value "auto" can be used
                      to detect the algorithm from the checksum. If missing, MD5 is
                      used. If in doubt, use "auto".
                    enum:
                    - md5
                    - sha256
                    - sha512
                    - auto
                    type: string
                  format:
                    description: DiskFormat contains the format of the image (raw,
                      qcow2, ...). Needs to be set to raw for raw images streaming.
                      Note live-iso means an iso referenced by the url will be live-booted
                      and not deployed to disk, and in this case the checksum options
                      are not required and if specified will be ignored.
                    enum:
                    - raw
                    - qcow2
                    - vdi
                    - vmdk
                    - live-iso
                    type: string
                  url:
                    description: URL is a location of an image to deploy.
                    type: string
                required:
                - url
                type: object
              kind:
                description: Kind identifies the target of the host. If not set will
                  default to host.kanod.io/kind selector
                type: string
              metaData:
                description: MetaData holds the reference to the Secret containing
                  host metadata (e.g. meta_data.json) which is passed to the Config
                  Drive.
                properties:
                  name:
                    description: name is unique within a namespace to reference a
                      secret resource.
                    type: string
                  namespace:
                    description: namespace defines the space within which the secret
                      name must be unique.
                    type: string
                type: object
                x-kubernetes-map-type: atomic
              networkData:
                description: NetworkData holds the reference to the Secret containing
                  network configuration (e.g content of network_data.json) which is
                  passed to the Config Drive.
                properties:
                  name:
                    description: name is unique within a namespace to reference a
                      secret resource.
                    type: string
                  namespace:
                    description: namespace defines the space within which the secret
                      name must be unique.
                    type: string
                type: object
                x-kubernetes-map-type: atomic
              online:
                description: Should the server be online?
                type: boolean
              userData:
                description: UserData holds the reference to the Secret containing
                  the user data to be passed to the host before it boots.
                properties:
                  name:
                    description: name is unique within a namespace to reference a
                      secret resource.
                    type: string
                  namespace:
                    description: namespace defines the space within which the secret
                      name must be unique.
                    type: string
                type: object
                x-kubernetes-map-type: atomic
            required:
            - online
            type: object
          status:
            description: HostStatus defines the observed state of Host
            properties:
              addresses:
                description: Addresses is a list of addresses assigned to the machine.
                  This field is copied from the infrastructure provider reference.
                items:
                  description: HostAddress contains information for the node's address.
                  properties:
                    address:
                      description: The machine address.
                      type: string
                    type:
                      description: Host address type, one of Hostname, ExternalIP,
                        InternalIP, ExternalDNS or InternalDNS.
                      type: string
                  required:
                  - address
                  - type
                  type: object
                type: array
              bootMACAddress:
                description: The MACAddress of the interface used to boot the underlying
                  compute resource
                type: string
              conditions:
                description: Conditions defines current service state of the Host.
                items:
                  description: Condition defines an observation of a Cluster API resource
                    operational state.
                  properties:
                    lastTransitionTime:
                      description: Last time the condition transitioned from one status
                        to another. This should be when the underlying condition changed.
                        If that is not known, then using the time when the API field
                        changed is acceptable.
                      format: date-time
                      type: string
                    message:
                      description: A human readable message indicating details about
                        the transition. This field may be empty.
                      type: string
                    reason:
                      description: The reason for the condition's last transition
                        in CamelCase. The specific API may choose whether or not this
                        field is considered a guaranteed API. This field may not be
                        empty.
                      type: string
                    severity:
                      description: Severity provides an explicit classification of
                        Reason code, so the users or machines can immediately understand
                        the current situation and act accordingly. The Severity field
                        MUST be set only when Status=False.
                      type: string
                    status:
                      description: Status of the condition, one of True, False, Unknown.
                      type: string
                    type:
                      description: Type of condition in CamelCase or in foo.example.com/CamelCase.
                        Many .condition.type values are consistent across resources
                        like Available, but because arbitrary conditions can be useful
                        (see .node.status.conditions), the ability to deconflict is
                        important.
                      type: string
                  required:
                  - lastTransitionTime
                  - status
                  - type
                  type: object
                type: array
              failureMessage:
                description: FailureMessage will be set in the event that there is
                  a terminal problem reconciling the host and will contain a more
                  verbose string suitable for logging and human consumption.
                type: string
              failureReason:
                description: "FailureReason will be set in the event that there is
                  a terminal problem reconciling the host and will contain a succinct
                  value suitable for machine interpretation. \n This field should
                  not be set for transitive errors that a controller faces that are
                  expected to be fixed automatically over time (like service outages),
                  but instead indicate that something is fundamentally wrong with
                  the host's spec or the configuration of the controller, and that
                  manual intervention is required. \n Any transient errors that occur
                  during the reconciliation of host can be added as events to the
                  host object and/or logged in the controller's output."
                type: string
              hostUID:
                description: HostUID is the UID of the underlying compute. It is used
                  as ID by the capm3 controller.
                type: string
              lastUpdated:
                description: LastUpdated identifies when this status was last observed.
                format: date-time
                type: string
              nics:
                description: The network interfaces on the underlying compute resource
                items:
                  properties:
                    MAC:
                      description: The device MAC address
                      type: string
                    ip:
                      description: The IP address of the interface. This will be an
                        IPv4 or IPv6 address if one is present.  If both IPv4 and
                        IPv6 addresses are present in a dual-stack environment, two
                        nics will be output, one with each IP.
                      type: string
                    name:
                      description: The name of the network interface, e.g. "en0"
                      type: string
                    pxe:
                      description: Whether the NIC is PXE Bootable
                      type: boolean
                  required:
                  - MAC
                  - name
                  type: object
                type: array
              ready:
                description: Ready is the state of the host It means the reconciliation
                  completed.
                type: boolean
              synced:
                description: Synced means the metaData of the host is synced with
                  the compute resource
                type: boolean
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
