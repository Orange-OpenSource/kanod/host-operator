.. _host_baremetal:

The Bare-metal target for Host resource
=======================================
The first target for the Host custom resource is the BareMetalHost resource.
When the Host resource contains a selector:

.. code-block:: yaml

    hostSelector:
      matchLabels:
        host.kanod.io/kind: baremetal
        label1: value1
      matchExpressions:
      - key: label2
        operator: in
        values: ['a', 'b', 'c']

the controller will try to find a BareMetalHost resource that
fulfills the following hostSelector:

.. code-block:: yaml

    hostSelector:
      matchLabels:
        label1: value1
      matchExpressions:
      - key: label2
        operator: in
        values: ['a', 'b', 'c']

in the cluster (it must have a label ``label1``with value ``value1`` and a label
``label2`` with value either ``a`` ``b`` or ``c``)  and associate it with the host.

The associated BareMetalHost can be in another namespace. In the current version
there is no mechanism to control/prevent the association of a Host with a BareMetalHost.
We may add an annotation on the BareMetalHost to restrict the namespaces from which
the Host resources can bind a specific bare-metal.

Association for the BareMetalHost means:

* the values of the specification fields ``image``, ``userData``, ``metaData`` and
  ``networkData`` are copied from the Host resource.
* the ``online`` field reflects the value of the Host resource.
* the ``consumerRef`` of the BareMetalHost resource is set to point to the Host
  resource.

Association for the Host means:

* For each label whose name appear in the annotation ``kanod.io/exported-labels``
  (a comma separated list of values)
  of the BareMetalHost, the label value on the Host is synchronized with the
  label value on the BareMetalHost.
* For each annotation whose name appear in the annotation ``kanod.io/exported-annotations``
  of the BareMetalHost, the annotation value on the Host is synchronized with the
  annotation value on the BareMetalHost.
* the status of the Host reflects part of the status of the BareMetalHost:

  * ``addresses``, and ``nics`` from the ``hardwareDetails`` in the status
    are propagated back to the Host,
  * the specified ``bootMacAddress``  is propagated back to the Host,
  * hostUID is set to the UID of the BareMetalHost when the BareMetalHost is
    provisioned.

