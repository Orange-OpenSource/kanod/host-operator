.. _host_capm3:

Using host resource with Cluster-Api
====================================
Hosts can be used with a modified version of the cluster-api provider Metal3
for bare-metal servers. The host custom resource provides in fact an
additional API contract between the Metal3 machine and the bare-metal host to
which it is linked. The combination of the code of the Metal3 machine controller
targeting host and the host controller targeting bare-metal hosts is strictly
equivalent to the original code of the metal3 machine controller (whose target
is bare-metal hosts).

Adding this intermediate resource solves two problems:

* Multi tenancy: several users sharing a set of bare-metal compute resources to
  build clusters that are kept isolated.
* Hybrid clusters: clusters built with different kind of compute resources as
  long as they can be defined and configured by an OS image and a cloud-init
  configuration.

Operation
---------
The modified capm3 provider looks for an annotation ``kanod.io/use-host``. If
it is present, associated Metal3 machines (for machine deployments and
control-plane) will be associated to Host resources and not to BareMetalHost.
A Host resource is created for each Metal3 machine and its life-cycle follows
the life-cycle of the machine. Its specification is deduced from the Metal3
machine:

* It roughly follows what is set by a Metal3 machine on a BareMetalHost
  when the traditional controller is used.
* It also inherit a copy of the host selector specified in the Metal3 machine.

Interaction with Data Template
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Host resource replaces the BareMetalHost in data template. This is why
it is important that Host inherit labels and annotations from their associated
BareMetalHost resource.

Some small adaptation of the controller were required but their impact is mostly
invisible for the end-user.

Interaction with remediation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Currently, Hosts are incompatible with capm3 remediation because they lack
a mechanism to force a reboot of the underlying resource. It may be hard to
support it on some of the controllers.

Multi-Tenancy
-------------
it is not necessary to have the Host and the BareMetalHost in
the same namespace. Host provides sufficient control and view over the
BareMetalHost so that the host user does not need a direct access to the
bare-metal server and does not need to know the BMC credentials.

As long as the controller requires a valid consumer definition in the BareMetalHost
to act on it, the customer may have full control over hosts resources, he cannot
hijack a BareMetalHost that does not belong to him.

In the current prototype, there is no contract to specify quotas over compute
resources. It may be implemented at a later stage as just
an admission plugin over host resources.

Hybrid clusters
---------------
Because hosts are relatively abstract structures, they may target different kinds
of compute resources and not only bare-metal servers. For example host controllers
have been developed for Openstack virtual machines and for Kubevirt virtual machines
deployed inside the management cluster.

The main mechanism is the use of a label selector over "virtual" label
``host.kanod.io/kind`` to select the target kind and the controller implementing
the reconciliation.

Metal3 has never addressed network configuration and so do host controllers. It may
be necessary to configure additional resources to enable communication between the
different kind of hosts.




