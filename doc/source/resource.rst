.. _host_resource:

The Host resource
=================

The ``Host`` custom resource is a new Kubernetes resource that represents a
compute resource that can be provisioned with an OS image and initialized
with a `cloud-init <https://cloudinit.readthedocs.io>`_ configuration. The
compute resource can either be:

* recyclable: eg a baremetal server that can be reused by another host
  when it is not needed anymore.
* disposable: eg a virtual machine that is destroyed when the host resource
  is destroyed.

The choice of the compute resource is configurable through a selector mechanism
based on the values of labels.

Use cases
----------

Direct usage
^^^^^^^^^^^^

A user may want to start a server with a custom OS image and not use it as part
of a cluster. He could directly specify a BareMetalHost, but this also means
that it chooses exactly which server to use and has access to the full configuration
of the server. This is not necessary, the user may express the requirements on
the underlying server with constraints as is done in the capm3 provider.

The host controllers will select the right compute resource. In the case of
bare-metal servers this enable a clean separation between teams managing the
servers and teams using them.

Multi Tenancy for Metal3
^^^^^^^^^^^^^^^^^^^^^^^^

We may want to use one single life-cycle manager controlling a set of bare-metal
servers to create clusters for different users. With the current implementation
of Metal3, clusters and bareMetalHost must be in the same namespace. This means
that if the pool of servers is shared, all the clusters must be in the same
namespace. It is then difficult to ensure isolation between clusters specification
with Kubernetes RBAC. It is also necessary to restrict the access to BareMetalHost
resources because having the control over the BMC means having a full control
over the server.

Using Host resources, we can solve this problem by using a different namespace for
each cluster. The Host resources are created in the same namespace as the cluster
but target real computing resources elsewhere.

Hybrid Cluster API clusters
^^^^^^^^^^^^^^^^^^^^^^^^^^^

In some scenarios, it may be interesting to create clusters with different kind
of nodes:

* The control-plane could be deployed directly in the life-cycle manager with
  lightweight virtual machines for example.
* Some deployments would benefit to be directly on bare-metal servers typically
  for virtual network functions.
* Other nodes could be implemented as virtual machines in on premise or external
  public clouds.

The host resource is relatively abstract and only specify an OS image and a cloud-init
configuration. As long as the compute resource can be defined with those characteristics
a controller targetting this compute resource can be defined. From Cluster API
point of view every node is defined on a standard machine and the only difference
is a set of requirements in the hostSelector part of the Metal3Machine template.


Host specification
------------------

Here is an example of Host specification:

.. code-block:: yaml

    apiVersion: kanod.io/v1alpha1
    kind: Host
    metadata:
      name: host1
    spec:
      hostSelector:
        matchLabels:
          host.kanod.io/kind: kubevirt
          host.kanod.io/cores: 4
      image:
        checksum: https://maven.service.kanod.home.arpa/repository/kanod/kanod/jammy-v1.24.15/1.13.26/jammy-v1.24.15-1.13.26.qcow2.md5
        checksumType: md5
        format: qcow2
        url: https://maven.service.kanod.home.arpa/repository/kanod/kanod/jammy-v1.24.15/1.13.26/jammy-v1.24.15-1.13.26.qcow2
      metaData:
        name: host1-metadata
      online: true
      userData:
        name: host1-userdata

The ``hostSelector`` field is used to select the compute resource: in the example
it is a kubevirt virtual machine in the life-cycle manager cluster with 4 cores
and default memory and storage values.

The ``image`` field specifies the OS image to use with its format and checksum,
``userData`` and ``metaData`` are pointers on secrets containing cloud-init user-data
and meta-data. Those secrets follow the same conventions as for BareMetalHosts:

* the meta-data secret contains one field metaData whose content is a list of
  meta-data bindings (one per line, key: value format)
* the user-data secret contains a field ``format`` and a field ``value``.
  The value of format is ``cloud-config``

**Warning:** CoreOs should be supported when the host target is baremetal but
is not supported in most other cases (OpenStack or Kubevirt).
