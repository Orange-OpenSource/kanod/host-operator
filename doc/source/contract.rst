.. _host_contract:

The Host controller contract
============================
Controllers for the host resource must follow a contract that ensures
compatibility with host users such as the cluster-api baremetal controller
(Metal3).

Host selection
--------------
Host selection is done through the selector field. It uses the same syntax
as the one in Metal3Machine with two parts:

* an explicit match on values,
* match expressions.

Although it is a label selector, the mechanism will be tricked to handle special
cases when the selection is explicit. Labels with domain "host.kanod.io" have
a special meaning and cannot be used as regular labels but are interpreted
by the different controllers as requirements.

* 'host.kanod.io/kind' is reserved to define the target and so the controller
  handling the host. An example value is 'baremetal' for host targetting Metal3
  BareMetalHost.
* 'host.kanod.io/virtual' if it is defined contains the name of the
  configuration secret for synchronizing the host resource with another host
  on a different cluster. Host resource controller MUST check the presence of
  this entry and refuse to reconcile hosts for which it is defined.
* Other labels in this domain are interpreted as configuration values by
  controllers for disposable targets (virtual machines).

Labels in other domains are interpreted as real constraints by
controllers for recyclable targets and are just propagated for disposable ones.

Host status
-----------

The controllers implementing reconciliation for various host targets should try
to give back information on the network configuration of the host as needed by
the data template resource:

* network interface controllers available on the compute resource should be
  exposed in the ``nics`` field of status.
* ip addresses configured on the compute resource should be exposed in addresses
* the mac address of the NIC used for boot should be exposed.

Transient Host status
---------------------
It may be necessary to reboot the associated compute resource to try to recover
from errors. This will be implemented as an annotation on the host resource.
When the action is performed, the controller must remove the annotation.

**Warning:** not yet implemented.
