=====================
Host Custom Resource
=====================

.. toctree::
    :maxdepth: 2

    resource
    baremetal
    capm3
    contract
    api
